<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;


class RegisterController extends Controller
{
    public function register()
    {
        return view('Auth.register');
    }
 
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'name' => 'required|max:255',
            'email' => 'required|unique:users',
            'password' => 'required',
        ]);
 
        $validatedData['password'] = Hash::make($validatedData['password']);
 
        User::create($validatedData);
        return redirect('/login')->with('success', 'Registration Succesfull! Please Login');
    }

    public function login()
    {
        return view('Auth.login');
    }

    public function dashboard()
    {
        return view('Home.dashboard');
    }

    public function instalasirawatjalan()
    {
        return view('Instalasi.instalasirawatjalan');
    }
}
